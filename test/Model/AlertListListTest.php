<?php
/**
 * AlertListListTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Gkeep API
 *
 * Gkeep API
 *
 * OpenAPI spec version: 0.0.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.15
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * AlertListListTest Class Doc Comment
 *
 * @category    Class
 * @description AlertListList
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class AlertListListTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "AlertListList"
     */
    public function testAlertListList()
    {
    }

    /**
     * Test attribute "fuel_level"
     */
    public function testPropertyFuelLevel()
    {
    }

    /**
     * Test attribute "fuel_level_on_raise_start"
     */
    public function testPropertyFuelLevelOnRaiseStart()
    {
    }

    /**
     * Test attribute "action_status"
     */
    public function testPropertyActionStatus()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "nav_system_object_uid"
     */
    public function testPropertyNavSystemObjectUid()
    {
    }

    /**
     * Test attribute "created_at"
     */
    public function testPropertyCreatedAt()
    {
    }

    /**
     * Test attribute "is_closed"
     */
    public function testPropertyIsClosed()
    {
    }

    /**
     * Test attribute "description"
     */
    public function testPropertyDescription()
    {
    }

    /**
     * Test attribute "vehicle"
     */
    public function testPropertyVehicle()
    {
    }

    /**
     * Test attribute "code"
     */
    public function testPropertyCode()
    {
    }

    /**
     * Test attribute "driver"
     */
    public function testPropertyDriver()
    {
    }

    /**
     * Test attribute "level"
     */
    public function testPropertyLevel()
    {
    }

    /**
     * Test attribute "status"
     */
    public function testPropertyStatus()
    {
    }

    /**
     * Test attribute "is_maintenance_enabled"
     */
    public function testPropertyIsMaintenanceEnabled()
    {
    }
}
