<?php
/**
 * UpdateProfileUserTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Gkeep API
 *
 * Gkeep API
 *
 * OpenAPI spec version: 0.0.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.15
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * UpdateProfileUserTest Class Doc Comment
 *
 * @category    Class
 * @description UpdateProfileUser
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class UpdateProfileUserTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "UpdateProfileUser"
     */
    public function testUpdateProfileUser()
    {
    }

    /**
     * Test attribute "firstname"
     */
    public function testPropertyFirstname()
    {
    }

    /**
     * Test attribute "lastname"
     */
    public function testPropertyLastname()
    {
    }

    /**
     * Test attribute "email"
     */
    public function testPropertyEmail()
    {
    }

    /**
     * Test attribute "phone"
     */
    public function testPropertyPhone()
    {
    }

    /**
     * Test attribute "comment"
     */
    public function testPropertyComment()
    {
    }

    /**
     * Test attribute "groups"
     */
    public function testPropertyGroups()
    {
    }

    /**
     * Test attribute "company"
     */
    public function testPropertyCompany()
    {
    }

    /**
     * Test attribute "company_short_name"
     */
    public function testPropertyCompanyShortName()
    {
    }

    /**
     * Test attribute "notify_timezone"
     */
    public function testPropertyNotifyTimezone()
    {
    }

    /**
     * Test attribute "lang"
     */
    public function testPropertyLang()
    {
    }

    /**
     * Test attribute "nav_system_access_data"
     */
    public function testPropertyNavSystemAccessData()
    {
    }
}
