<?php
/**
 * UserProfileListListTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Gkeep API
 *
 * Gkeep API
 *
 * OpenAPI spec version: 0.0.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.15
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * UserProfileListListTest Class Doc Comment
 *
 * @category    Class
 * @description UserProfileListList
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class UserProfileListListTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "UserProfileListList"
     */
    public function testUserProfileListList()
    {
    }

    /**
     * Test attribute "email"
     */
    public function testPropertyEmail()
    {
    }

    /**
     * Test attribute "groups"
     */
    public function testPropertyGroups()
    {
    }

    /**
     * Test attribute "locked"
     */
    public function testPropertyLocked()
    {
    }

    /**
     * Test attribute "firstname"
     */
    public function testPropertyFirstname()
    {
    }

    /**
     * Test attribute "lastname"
     */
    public function testPropertyLastname()
    {
    }

    /**
     * Test attribute "phone"
     */
    public function testPropertyPhone()
    {
    }

    /**
     * Test attribute "parent_id"
     */
    public function testPropertyParentId()
    {
    }

    /**
     * Test attribute "company"
     */
    public function testPropertyCompany()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "lang"
     */
    public function testPropertyLang()
    {
    }

    /**
     * Test attribute "geo_localization_settings"
     */
    public function testPropertyGeoLocalizationSettings()
    {
    }
}
