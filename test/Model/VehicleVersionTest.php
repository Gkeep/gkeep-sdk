<?php
/**
 * VehicleVersionTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Gkeep API
 *
 * Gkeep API
 *
 * OpenAPI spec version: 0.0.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.15
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Swagger\Client;

/**
 * VehicleVersionTest Class Doc Comment
 *
 * @category    Class
 * @description VehicleVersion
 * @package     Swagger\Client
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class VehicleVersionTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "VehicleVersion"
     */
    public function testVehicleVersion()
    {
    }

    /**
     * Test attribute "id"
     */
    public function testPropertyId()
    {
    }

    /**
     * Test attribute "name"
     */
    public function testPropertyName()
    {
    }

    /**
     * Test attribute "brand"
     */
    public function testPropertyBrand()
    {
    }

    /**
     * Test attribute "model"
     */
    public function testPropertyModel()
    {
    }

    /**
     * Test attribute "started_at"
     */
    public function testPropertyStartedAt()
    {
    }

    /**
     * Test attribute "ended_at"
     */
    public function testPropertyEndedAt()
    {
    }

    /**
     * Test attribute "tonnage"
     */
    public function testPropertyTonnage()
    {
    }

    /**
     * Test attribute "power"
     */
    public function testPropertyPower()
    {
    }

    /**
     * Test attribute "power_cv"
     */
    public function testPropertyPowerCv()
    {
    }

    /**
     * Test attribute "axes"
     */
    public function testPropertyAxes()
    {
    }
}
