/**
 * Gkeep API
 * Gkeep API
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 *
 */

import ApiClient from '../ApiClient';
import LastAlertListInner from './LastAlertListInner';

/**
* The LastAlertList model module.
* @module model/LastAlertList
* @version 0.0.1
*/
export default class LastAlertList extends Array {
    /**
    * Constructs a new <code>LastAlertList</code>.
    * @alias module:model/LastAlertList
    * @class
    * @extends Array
    */

    constructor() {
        super();
        
        
        return this;
    }

    /**
    * Constructs a <code>LastAlertList</code> from a plain JavaScript object, optionally creating a new instance.
    * Copies all relevant properties from <code>data</code> to <code>obj</code> if supplied or a new instance if not.
    * @param {Object} data The plain JavaScript object bearing properties of interest.
    * @param {module:model/LastAlertList} obj Optional instance to populate.
    * @return {module:model/LastAlertList} The populated <code>LastAlertList</code> instance.
    */
    static constructFromObject(data, obj) {
        if (data) {
            obj = obj || new LastAlertList();
            ApiClient.constructFromObject(data, obj, 'LastAlertListInner');
            
            
        }
        return obj;
    }





}
