# GkeepApi.LastAlertListInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**frameId** | **String** |  | [optional] 
**version** | **String** |  | [optional] 
**sensor** | **Object** |  | [optional] 
**imei** | **String** |  | [optional] 
**importStatus** | **Number** |  | [optional] 
**msgTime** | **Number** |  | [optional] 
**latitude** | **Number** |  | [optional] 
**longitude** | **Number** |  | [optional] 
**co2** | **Number** |  | [optional] 
**fuelConsumption** | **Number** |  | [optional] 
**fuelLevels** | [**[BigDecimal]**](BigDecimal.md) |  | [optional] 
**alignedFuelLevels** | **String** |  | [optional] 
**fuelLevel** | [**BigDecimal**](BigDecimal.md) |  | [optional] 
**vehicleId** | **Number** |  | [optional] 
**vehicle** | **Object** |  | [optional] 
**driverId** | **Number** |  | [optional] 
**isDayOff** | **Boolean** |  | [optional] 
**isMaintenanceEnabled** | **Boolean** |  | [optional] 
