/*
 * Gkeep API
 * Gkeep API
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

package io.swagger.client.api;

import io.swagger.client.ApiException;
import io.swagger.client.model.AlertList;
import io.swagger.client.model.FleetStatus;
import io.swagger.client.model.LastAlertList;
import io.swagger.client.model.UserProfile;
import io.swagger.client.model.VehicleDailyStats;
import io.swagger.client.model.VehicleList;
import io.swagger.client.model.VehicleStatus;
import org.junit.Test;
import org.junit.Ignore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * API tests for V2Api
 */
@Ignore
public class V2ApiTest {

    private final V2Api api = new V2Api();

    /**
     * 
     *
     * Get alerts
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getAlertsTest() throws ApiException {
        String filterCreatedDateStart = null;
        String filterCreatedDateEnd = null;
        Integer filterCodeSpecial = null;
        AlertList response = api.getAlerts(filterCreatedDateStart, filterCreatedDateEnd, filterCodeSpecial);

        // TODO: test validations
    }
    /**
     * 
     *
     * Get fleet last-infos
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getFletLastInfosTest() throws ApiException {
        FleetStatus response = api.getFletLastInfos();

        // TODO: test validations
    }
    /**
     * 
     *
     * Get last alerts
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getLastAlertsTest() throws ApiException {
        LastAlertList response = api.getLastAlerts();

        // TODO: test validations
    }
    /**
     * 
     *
     * Get user-profiles
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getUserProfileTest() throws ApiException {
        UserProfile response = api.getUserProfile();

        // TODO: test validations
    }
    /**
     * 
     *
     * Get vehicles
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getVehiclesTest() throws ApiException {
        VehicleList response = api.getVehicles();

        // TODO: test validations
    }
    /**
     * 
     *
     * Get vehicles daily
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getVehiclesDailyStatsTest() throws ApiException {
        Integer vehicleId = null;
        VehicleDailyStats response = api.getVehiclesDailyStats(vehicleId);

        // TODO: test validations
    }
    /**
     * 
     *
     * Get vehicles last-infos
     *
     * @throws ApiException
     *          if the Api call fails
     */
    @Test
    public void getVehiclesLatestStatsTest() throws ApiException {
        Integer vehicleId = null;
        VehicleStatus response = api.getVehiclesLatestStats(vehicleId);

        // TODO: test validations
    }
}
