# LastAlertListInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  |  [optional]
**frameId** | **String** |  |  [optional]
**version** | **String** |  |  [optional]
**sensor** | **Object** |  |  [optional]
**imei** | **String** |  |  [optional]
**importStatus** | **Integer** |  |  [optional]
**msgTime** | **Integer** |  |  [optional]
**latitude** | **Integer** |  |  [optional]
**longitude** | **Integer** |  |  [optional]
**co2** | **Integer** |  |  [optional]
**fuelConsumption** | **Integer** |  |  [optional]
**fuelLevels** | [**List&lt;BigDecimal&gt;**](BigDecimal.md) |  |  [optional]
**alignedFuelLevels** | **String** |  |  [optional]
**fuelLevel** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**vehicleId** | **Integer** |  |  [optional]
**vehicle** | **Object** |  |  [optional]
**driverId** | **Integer** |  |  [optional]
**isDayOff** | **Boolean** |  |  [optional]
**isMaintenanceEnabled** | **Boolean** |  |  [optional]
