# VehicleListList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gkeep_id** | **string** |  | [optional] 
**id** | **int** |  | [optional] 
**owner** | [**\Swagger\Client\Model\AlertListVehicleOwner**](AlertListVehicleOwner.md) |  | [optional] 
**driver** | [**\Swagger\Client\Model\AlertListDriver**](AlertListDriver.md) |  | [optional] 
**groups** | [**\Swagger\Client\Model\VehicleListGroups[]**](VehicleListGroups.md) |  | [optional] 
**name** | **string** |  | [optional] 
**photo** | **string** |  | [optional] 
**sensor** | [**\Swagger\Client\Model\VehicleListSensor**](VehicleListSensor.md) |  | [optional] 
**last_alert_at** | **int** |  | [optional] 
**first_frame_at** | **int** |  | [optional] 
**is_maintenance_enabled** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

