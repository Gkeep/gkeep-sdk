# UserProfile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **string** |  | [optional] 
**groups** | [**\Swagger\Client\Model\UserProfileGroups[]**](UserProfileGroups.md) |  | [optional] 
**roles** | **string[]** |  | [optional] 
**firstname** | **string** |  | [optional] 
**lastname** | **string** |  | [optional] 
**phone** | **string** |  | [optional] 
**parent_id** | **string** |  | [optional] 
**association** | **string** |  | [optional] 
**comment** | **string** |  | [optional] 
**company** | **string** |  | [optional] 
**company_short_name** | **string** |  | [optional] 
**contract** | **string** |  | [optional] 
**external_company** | **string** |  | [optional] 
**nav_system_access_data** | **string** |  | [optional] 
**id** | **int** |  | [optional] 
**lang** | [**\Swagger\Client\Model\UserProfileLang**](UserProfileLang.md) |  | [optional] 
**nav_system** | **string** |  | [optional] 
**notify_by_email** | **bool** |  | [optional] 
**notify_by_sms** | **bool** |  | [optional] 
**notify_timezone** | **string** |  | [optional] 
**partner_type** | **string** |  | [optional] 
**is_terms_accepted** | **bool** |  | [optional] 
**geo_localization_settings** | [**\Swagger\Client\Model\UserProfileGeoLocalizationSettings**](UserProfileGeoLocalizationSettings.md) |  | [optional] 
**api_token** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

