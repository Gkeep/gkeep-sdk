<?php
/**
 * VehicleStatusTanks
 *
 * PHP version 5
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Gkeep API
 *
 * Gkeep API
 *
 * OpenAPI spec version: 0.0.1
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.15
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Swagger\Client\Model;

use \ArrayAccess;
use \Swagger\Client\ObjectSerializer;

/**
 * VehicleStatusTanks Class Doc Comment
 *
 * @category Class
 * @package  Swagger\Client
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class VehicleStatusTanks implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'VehicleStatus_tanks';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'fuel_level' => 'float',
'fuel_level_on_raise_start' => 'float',
'tank' => '\Swagger\Client\Model\VehicleStatusTank'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'fuel_level' => null,
'fuel_level_on_raise_start' => null,
'tank' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'fuel_level' => 'fuel_level',
'fuel_level_on_raise_start' => 'fuel_level_on_raise_start',
'tank' => 'tank'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'fuel_level' => 'setFuelLevel',
'fuel_level_on_raise_start' => 'setFuelLevelOnRaiseStart',
'tank' => 'setTank'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'fuel_level' => 'getFuelLevel',
'fuel_level_on_raise_start' => 'getFuelLevelOnRaiseStart',
'tank' => 'getTank'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['fuel_level'] = isset($data['fuel_level']) ? $data['fuel_level'] : null;
        $this->container['fuel_level_on_raise_start'] = isset($data['fuel_level_on_raise_start']) ? $data['fuel_level_on_raise_start'] : null;
        $this->container['tank'] = isset($data['tank']) ? $data['tank'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets fuel_level
     *
     * @return float
     */
    public function getFuelLevel()
    {
        return $this->container['fuel_level'];
    }

    /**
     * Sets fuel_level
     *
     * @param float $fuel_level fuel_level
     *
     * @return $this
     */
    public function setFuelLevel($fuel_level)
    {
        $this->container['fuel_level'] = $fuel_level;

        return $this;
    }

    /**
     * Gets fuel_level_on_raise_start
     *
     * @return float
     */
    public function getFuelLevelOnRaiseStart()
    {
        return $this->container['fuel_level_on_raise_start'];
    }

    /**
     * Sets fuel_level_on_raise_start
     *
     * @param float $fuel_level_on_raise_start fuel_level_on_raise_start
     *
     * @return $this
     */
    public function setFuelLevelOnRaiseStart($fuel_level_on_raise_start)
    {
        $this->container['fuel_level_on_raise_start'] = $fuel_level_on_raise_start;

        return $this;
    }

    /**
     * Gets tank
     *
     * @return \Swagger\Client\Model\VehicleStatusTank
     */
    public function getTank()
    {
        return $this->container['tank'];
    }

    /**
     * Sets tank
     *
     * @param \Swagger\Client\Model\VehicleStatusTank $tank tank
     *
     * @return $this
     */
    public function setTank($tank)
    {
        $this->container['tank'] = $tank;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
