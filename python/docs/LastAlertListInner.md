# LastAlertListInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**frame_id** | **str** |  | [optional] 
**version** | **str** |  | [optional] 
**sensor** | **object** |  | [optional] 
**imei** | **str** |  | [optional] 
**import_status** | **int** |  | [optional] 
**msg_time** | **int** |  | [optional] 
**latitude** | **int** |  | [optional] 
**longitude** | **int** |  | [optional] 
**co2** | **int** |  | [optional] 
**fuel_consumption** | **int** |  | [optional] 
**fuel_levels** | **list[float]** |  | [optional] 
**aligned_fuel_levels** | **str** |  | [optional] 
**fuel_level** | **float** |  | [optional] 
**vehicle_id** | **int** |  | [optional] 
**vehicle** | **object** |  | [optional] 
**driver_id** | **int** |  | [optional] 
**is_day_off** | **bool** |  | [optional] 
**is_maintenance_enabled** | **bool** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

