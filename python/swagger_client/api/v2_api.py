# coding: utf-8

"""
    Gkeep API

    Gkeep API  # noqa: E501

    OpenAPI spec version: 0.0.1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from swagger_client.api_client import ApiClient


class V2Api(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def get_alerts(self, **kwargs):  # noqa: E501
        """get_alerts  # noqa: E501

        Get alerts  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_alerts(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str filter_created_date_start: Start date of query selection
        :param str filter_created_date_end: End date of query selection
        :param int filter_code_special: Alert code id:  * `2` - fuel_is_missing  * `3` - no_alimentation  * `4` - keeper_not_connected  * `8` - safety_battery_level_under_30  * `9` - fuel_level_rise 
        :return: AlertList
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_alerts_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.get_alerts_with_http_info(**kwargs)  # noqa: E501
            return data

    def get_alerts_with_http_info(self, **kwargs):  # noqa: E501
        """get_alerts  # noqa: E501

        Get alerts  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_alerts_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str filter_created_date_start: Start date of query selection
        :param str filter_created_date_end: End date of query selection
        :param int filter_code_special: Alert code id:  * `2` - fuel_is_missing  * `3` - no_alimentation  * `4` - keeper_not_connected  * `8` - safety_battery_level_under_30  * `9` - fuel_level_rise 
        :return: AlertList
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['filter_created_date_start', 'filter_created_date_end', 'filter_code_special']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_alerts" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []
        if 'filter_created_date_start' in params:
            query_params.append(('filter[created][date_start]', params['filter_created_date_start']))  # noqa: E501
        if 'filter_created_date_end' in params:
            query_params.append(('filter[created][date_end]', params['filter_created_date_end']))  # noqa: E501
        if 'filter_code_special' in params:
            query_params.append(('filter[code_special]', params['filter_code_special']))  # noqa: E501

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['bearerAuth']  # noqa: E501

        return self.api_client.call_api(
            '/api/v2/alerts', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='AlertList',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_flet_last_infos(self, **kwargs):  # noqa: E501
        """get_flet_last_infos  # noqa: E501

        Get fleet last-infos  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_flet_last_infos(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: FleetStatus
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_flet_last_infos_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.get_flet_last_infos_with_http_info(**kwargs)  # noqa: E501
            return data

    def get_flet_last_infos_with_http_info(self, **kwargs):  # noqa: E501
        """get_flet_last_infos  # noqa: E501

        Get fleet last-infos  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_flet_last_infos_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: FleetStatus
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = []  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_flet_last_infos" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['bearerAuth']  # noqa: E501

        return self.api_client.call_api(
            '/api/v2/fleet/last-infos', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='FleetStatus',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_last_alerts(self, **kwargs):  # noqa: E501
        """get_last_alerts  # noqa: E501

        Get last alerts  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_last_alerts(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: LastAlertList
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_last_alerts_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.get_last_alerts_with_http_info(**kwargs)  # noqa: E501
            return data

    def get_last_alerts_with_http_info(self, **kwargs):  # noqa: E501
        """get_last_alerts  # noqa: E501

        Get last alerts  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_last_alerts_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: LastAlertList
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = []  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_last_alerts" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['bearerAuth']  # noqa: E501

        return self.api_client.call_api(
            '/api/v2/alerts/last', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='LastAlertList',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_user_profile(self, **kwargs):  # noqa: E501
        """get_user_profile  # noqa: E501

        Get user-profiles  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_user_profile(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: UserProfile
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_user_profile_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.get_user_profile_with_http_info(**kwargs)  # noqa: E501
            return data

    def get_user_profile_with_http_info(self, **kwargs):  # noqa: E501
        """get_user_profile  # noqa: E501

        Get user-profiles  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_user_profile_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: UserProfile
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = []  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_user_profile" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['bearerAuth']  # noqa: E501

        return self.api_client.call_api(
            '/api/v2/user-profiles', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='UserProfile',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_vehicles(self, **kwargs):  # noqa: E501
        """get_vehicles  # noqa: E501

        Get vehicles  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_vehicles(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: VehicleList
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_vehicles_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.get_vehicles_with_http_info(**kwargs)  # noqa: E501
            return data

    def get_vehicles_with_http_info(self, **kwargs):  # noqa: E501
        """get_vehicles  # noqa: E501

        Get vehicles  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_vehicles_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: VehicleList
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = []  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_vehicles" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['bearerAuth']  # noqa: E501

        return self.api_client.call_api(
            '/api/v2/vehicles', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='VehicleList',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_vehicles_daily_stats(self, vehicle_id, **kwargs):  # noqa: E501
        """get_vehicles_daily_stats  # noqa: E501

        Get vehicles daily  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_vehicles_daily_stats(vehicle_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int vehicle_id: vehicle identifier (required)
        :return: VehicleDailyStats
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_vehicles_daily_stats_with_http_info(vehicle_id, **kwargs)  # noqa: E501
        else:
            (data) = self.get_vehicles_daily_stats_with_http_info(vehicle_id, **kwargs)  # noqa: E501
            return data

    def get_vehicles_daily_stats_with_http_info(self, vehicle_id, **kwargs):  # noqa: E501
        """get_vehicles_daily_stats  # noqa: E501

        Get vehicles daily  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_vehicles_daily_stats_with_http_info(vehicle_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int vehicle_id: vehicle identifier (required)
        :return: VehicleDailyStats
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['vehicle_id']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_vehicles_daily_stats" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'vehicle_id' is set
        if ('vehicle_id' not in params or
                params['vehicle_id'] is None):
            raise ValueError("Missing the required parameter `vehicle_id` when calling `get_vehicles_daily_stats`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'vehicle_id' in params:
            path_params['vehicleId'] = params['vehicle_id']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['bearerAuth']  # noqa: E501

        return self.api_client.call_api(
            '/api/v2/vehicles/{vehicleId}/daily', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='VehicleDailyStats',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_vehicles_latest_stats(self, vehicle_id, **kwargs):  # noqa: E501
        """get_vehicles_latest_stats  # noqa: E501

        Get vehicles last-infos  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_vehicles_latest_stats(vehicle_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int vehicle_id: vehicle identifier (required)
        :return: VehicleStatus
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_vehicles_latest_stats_with_http_info(vehicle_id, **kwargs)  # noqa: E501
        else:
            (data) = self.get_vehicles_latest_stats_with_http_info(vehicle_id, **kwargs)  # noqa: E501
            return data

    def get_vehicles_latest_stats_with_http_info(self, vehicle_id, **kwargs):  # noqa: E501
        """get_vehicles_latest_stats  # noqa: E501

        Get vehicles last-infos  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_vehicles_latest_stats_with_http_info(vehicle_id, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param int vehicle_id: vehicle identifier (required)
        :return: VehicleStatus
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['vehicle_id']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_vehicles_latest_stats" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'vehicle_id' is set
        if ('vehicle_id' not in params or
                params['vehicle_id'] is None):
            raise ValueError("Missing the required parameter `vehicle_id` when calling `get_vehicles_latest_stats`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'vehicle_id' in params:
            path_params['vehicleId'] = params['vehicle_id']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = ['bearerAuth']  # noqa: E501

        return self.api_client.call_api(
            '/api/v2/vehicles/{vehicleId}/last-infos', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='VehicleStatus',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)
