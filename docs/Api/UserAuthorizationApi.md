# Swagger\Client\UserAuthorizationApi

All URIs are relative to *https://staging-transport.smart-gamma.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**authorize**](UserAuthorizationApi.md#authorize) | **POST** /api/login_check | 

# **authorize**
> \Swagger\Client\Model\AuthorizedUser authorize($body)



User authorization

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\UserAuthorizationApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$body = new \Swagger\Client\Model\UserCredentials(); // \Swagger\Client\Model\UserCredentials | A JSON object containing user credentials info

try {
    $result = $apiInstance->authorize($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UserAuthorizationApi->authorize: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\UserCredentials**](../Model/UserCredentials.md)| A JSON object containing user credentials info |

### Return type

[**\Swagger\Client\Model\AuthorizedUser**](../Model/AuthorizedUser.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

