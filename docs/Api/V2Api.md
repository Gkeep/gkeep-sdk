# Swagger\Client\V2Api

All URIs are relative to *https://staging-transport.smart-gamma.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**createUserProfile**](V2Api.md#createuserprofile) | **POST** /api/v2/user-profiles | 
[**createVehicleCategory**](V2Api.md#createvehiclecategory) | **POST** /api/v2/vehicles/categories | 
[**deleteUserProfile**](V2Api.md#deleteuserprofile) | **DELETE** /api/v2/user-profiles/{profileId} | 
[**deleteVehicleCategory**](V2Api.md#deletevehiclecategory) | **DELETE** /api/v2/vehicles/categories/{categoryId} | 
[**enableVehicleMaintenance**](V2Api.md#enablevehiclemaintenance) | **PUT** /api/v2/vehicles/maintenance/{vehicleId} | 
[**getAlertList**](V2Api.md#getalertlist) | **GET** /api/v2/alerts | 
[**getAlertsStatus**](V2Api.md#getalertsstatus) | **GET** /api/v2/alerts/status | 
[**getCurrentUserProfile**](V2Api.md#getcurrentuserprofile) | **GET** /api/v2/user-profiles | 
[**getFleetStatus**](V2Api.md#getfleetstatus) | **GET** /api/v2/fleet/status | 
[**getNotificationSettings**](V2Api.md#getnotificationsettings) | **GET** /api/v2/user-profiles/notification-settings | 
[**getRelatedUserProfiles**](V2Api.md#getrelateduserprofiles) | **GET** /api/v2/user-profiles/{levelName} | 
[**getUserProfile**](V2Api.md#getuserprofile) | **GET** /api/v2/user-profiles/{profileId} | 
[**getVehicleCategory**](V2Api.md#getvehiclecategory) | **GET** /api/v2/vehicles/categories/{categoryId} | 
[**getVehicleCategoryList**](V2Api.md#getvehiclecategorylist) | **GET** /api/v2/vehicles/categories | 
[**getVehicleStatus**](V2Api.md#getvehiclestatus) | **GET** /api/v2/vehicles/{vehicleId}/status | 
[**getVehicles**](V2Api.md#getvehicles) | **GET** /api/v2/vehicles | 
[**getVehiclesDailyStats**](V2Api.md#getvehiclesdailystats) | **GET** /api/v2/vehicles/{vehicleId}/daily-statistics | 
[**getVehiclesFrameHistory**](V2Api.md#getvehiclesframehistory) | **GET** /api/v2/vehicles/{vehicleId}/frame-history | 
[**getVehiclesRefuels**](V2Api.md#getvehiclesrefuels) | **GET** /api/v2/vehicles/{vehicleId}/refuels | 
[**getVehiclesStoppedConsumptions**](V2Api.md#getvehiclesstoppedconsumptions) | **GET** /api/v2/vehicles/{vehicleId}/stopped-consumptions | 
[**refreshApiToken**](V2Api.md#refreshapitoken) | **PUT** /api/v2/user-profiles/tokens/refresh | 
[**updateNotificationSettings**](V2Api.md#updatenotificationsettings) | **PUT** /api/v2/user-profiles/notification-settings | 
[**updateUserProfile**](V2Api.md#updateuserprofile) | **PUT** /api/v2/user-profiles/{profileId} | 
[**updateVehicle**](V2Api.md#updatevehicle) | **PUT** /api/v2/vehicles/{vehicleId} | 
[**updateVehicleCategory**](V2Api.md#updatevehiclecategory) | **PUT** /api/v2/vehicles/categories/{categoryId} | 

# **createUserProfile**
> \Swagger\Client\Model\UserProfile createUserProfile($body)



Create user profile

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\CreateProfile(); // \Swagger\Client\Model\CreateProfile | 

try {
    $result = $apiInstance->createUserProfile($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->createUserProfile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\CreateProfile**](../Model/CreateProfile.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\UserProfile**](../Model/UserProfile.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **createVehicleCategory**
> \Swagger\Client\Model\VehicleCategory createVehicleCategory($body)



Create vehicle category

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\CreateVehicleCategory(); // \Swagger\Client\Model\CreateVehicleCategory | 

try {
    $result = $apiInstance->createVehicleCategory($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->createVehicleCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\CreateVehicleCategory**](../Model/CreateVehicleCategory.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\VehicleCategory**](../Model/VehicleCategory.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteUserProfile**
> deleteUserProfile($profile_id)



Delete user profile

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$profile_id = 56; // int | ID

try {
    $apiInstance->deleteUserProfile($profile_id);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->deleteUserProfile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_id** | **int**| ID |

### Return type

void (empty response body)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **deleteVehicleCategory**
> deleteVehicleCategory($category_id)



Delete vehicle category

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$category_id = 56; // int | ID

try {
    $apiInstance->deleteVehicleCategory($category_id);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->deleteVehicleCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_id** | **int**| ID |

### Return type

void (empty response body)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **enableVehicleMaintenance**
> \Swagger\Client\Model\Vehicle enableVehicleMaintenance($vehicle_id, $body)



Enable vehicle maintenance mode

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$vehicle_id = 56; // int | Vehicle ID
$body = new \Swagger\Client\Model\VehicleMaintenance(); // \Swagger\Client\Model\VehicleMaintenance | 

try {
    $result = $apiInstance->enableVehicleMaintenance($vehicle_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->enableVehicleMaintenance: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **int**| Vehicle ID |
 **body** | [**\Swagger\Client\Model\VehicleMaintenance**](../Model/VehicleMaintenance.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\Vehicle**](../Model/Vehicle.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAlertList**
> \Swagger\Client\Model\AlertList getAlertList($filter_created_date_start, $filter_created_date_end, $filter_code_special)



Get alerts

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$filter_created_date_start = "filter_created_date_start_example"; // string | Start date of query selection
$filter_created_date_end = "filter_created_date_end_example"; // string | End date of query selection
$filter_code_special = 56; // int | Alert code id:  * `2` - fuel_is_missing  * `3` - no_alimentation  * `4` - keeper_not_connected  * `8` - safety_battery_level_under_30  * `9` - fuel_level_rise

try {
    $result = $apiInstance->getAlertList($filter_created_date_start, $filter_created_date_end, $filter_code_special);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->getAlertList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **filter_created_date_start** | **string**| Start date of query selection | [optional]
 **filter_created_date_end** | **string**| End date of query selection | [optional]
 **filter_code_special** | **int**| Alert code id:  * &#x60;2&#x60; - fuel_is_missing  * &#x60;3&#x60; - no_alimentation  * &#x60;4&#x60; - keeper_not_connected  * &#x60;8&#x60; - safety_battery_level_under_30  * &#x60;9&#x60; - fuel_level_rise | [optional]

### Return type

[**\Swagger\Client\Model\AlertList**](../Model/AlertList.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getAlertsStatus**
> \Swagger\Client\Model\AlertsStatus getAlertsStatus()



Get alerts status

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getAlertsStatus();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->getAlertsStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\AlertsStatus**](../Model/AlertsStatus.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getCurrentUserProfile**
> \Swagger\Client\Model\UserProfile getCurrentUserProfile()



Get current user profile

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getCurrentUserProfile();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->getCurrentUserProfile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\UserProfile**](../Model/UserProfile.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getFleetStatus**
> \Swagger\Client\Model\FleetStatus getFleetStatus()



Get fleet status

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getFleetStatus();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->getFleetStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\FleetStatus**](../Model/FleetStatus.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getNotificationSettings**
> \Swagger\Client\Model\NotificationSettings getNotificationSettings()



Get notification settings

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getNotificationSettings();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->getNotificationSettings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\NotificationSettings**](../Model/NotificationSettings.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getRelatedUserProfiles**
> \Swagger\Client\Model\UserProfileList getRelatedUserProfiles($level_name, $page, $per_page)



Get related user-profiles

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$level_name = new \Swagger\Client\Model\UserLevel(); // \Swagger\Client\Model\UserLevel | User Level
$page = "page_example"; // string | page
$per_page = "per_page_example"; // string | per_page

try {
    $result = $apiInstance->getRelatedUserProfiles($level_name, $page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->getRelatedUserProfiles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **level_name** | [**\Swagger\Client\Model\UserLevel**](../Model/.md)| User Level |
 **page** | **string**| page | [optional]
 **per_page** | **string**| per_page | [optional]

### Return type

[**\Swagger\Client\Model\UserProfileList**](../Model/UserProfileList.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getUserProfile**
> \Swagger\Client\Model\UserProfile getUserProfile($profile_id)



Get user-profiles

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$profile_id = 56; // int | ID

try {
    $result = $apiInstance->getUserProfile($profile_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->getUserProfile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_id** | **int**| ID |

### Return type

[**\Swagger\Client\Model\UserProfile**](../Model/UserProfile.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVehicleCategory**
> \Swagger\Client\Model\VehicleCategory getVehicleCategory($category_id)



Get vehicle category

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$category_id = 56; // int | ID

try {
    $result = $apiInstance->getVehicleCategory($category_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->getVehicleCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_id** | **int**| ID |

### Return type

[**\Swagger\Client\Model\VehicleCategory**](../Model/VehicleCategory.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVehicleCategoryList**
> \Swagger\Client\Model\VehicleCategoryList getVehicleCategoryList($page, $per_page)



Get vehicle category list

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$page = "page_example"; // string | page
$per_page = "per_page_example"; // string | per_page

try {
    $result = $apiInstance->getVehicleCategoryList($page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->getVehicleCategoryList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **string**| page | [optional]
 **per_page** | **string**| per_page | [optional]

### Return type

[**\Swagger\Client\Model\VehicleCategoryList**](../Model/VehicleCategoryList.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVehicleStatus**
> \Swagger\Client\Model\VehicleStatus getVehicleStatus($vehicle_id)



Get vehicle status

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$vehicle_id = 56; // int | vehicle identifier

try {
    $result = $apiInstance->getVehicleStatus($vehicle_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->getVehicleStatus: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **int**| vehicle identifier |

### Return type

[**\Swagger\Client\Model\VehicleStatus**](../Model/VehicleStatus.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVehicles**
> \Swagger\Client\Model\VehicleList getVehicles()



Get vehicles

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->getVehicles();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->getVehicles: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\VehicleList**](../Model/VehicleList.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVehiclesDailyStats**
> \Swagger\Client\Model\VehicleDailyStats getVehiclesDailyStats($vehicle_id, $filters_started_at, $filters_ended_at)



Get vehicles daily statistics

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$vehicle_id = 56; // int | vehicle identifier
$filters_started_at = "filters_started_at_example"; // string | filters[started_at]
$filters_ended_at = "filters_ended_at_example"; // string | filters[ended_at]

try {
    $result = $apiInstance->getVehiclesDailyStats($vehicle_id, $filters_started_at, $filters_ended_at);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->getVehiclesDailyStats: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **int**| vehicle identifier |
 **filters_started_at** | **string**| filters[started_at] | [optional]
 **filters_ended_at** | **string**| filters[ended_at] | [optional]

### Return type

[**\Swagger\Client\Model\VehicleDailyStats**](../Model/VehicleDailyStats.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVehiclesFrameHistory**
> \Swagger\Client\Model\FrameHistory getVehiclesFrameHistory($vehicle_id, $filters_started_at, $filters_ended_at, $page, $per_page)



Get vehicles frame-history

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$vehicle_id = 56; // int | vehicle identifier
$filters_started_at = "filters_started_at_example"; // string | filters[started_at]
$filters_ended_at = "filters_ended_at_example"; // string | filters[ended_at]
$page = "page_example"; // string | page
$per_page = "per_page_example"; // string | per_page

try {
    $result = $apiInstance->getVehiclesFrameHistory($vehicle_id, $filters_started_at, $filters_ended_at, $page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->getVehiclesFrameHistory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **int**| vehicle identifier |
 **filters_started_at** | **string**| filters[started_at] | [optional]
 **filters_ended_at** | **string**| filters[ended_at] | [optional]
 **page** | **string**| page | [optional]
 **per_page** | **string**| per_page | [optional]

### Return type

[**\Swagger\Client\Model\FrameHistory**](../Model/FrameHistory.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVehiclesRefuels**
> \Swagger\Client\Model\RefuelsList getVehiclesRefuels($vehicle_id, $filters_started_at, $filters_ended_at)



Get vehicles refuels

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$vehicle_id = 56; // int | vehicle identifier
$filters_started_at = "filters_started_at_example"; // string | filters[started_at]
$filters_ended_at = "filters_ended_at_example"; // string | filters[ended_at]

try {
    $result = $apiInstance->getVehiclesRefuels($vehicle_id, $filters_started_at, $filters_ended_at);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->getVehiclesRefuels: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **int**| vehicle identifier |
 **filters_started_at** | **string**| filters[started_at] | [optional]
 **filters_ended_at** | **string**| filters[ended_at] | [optional]

### Return type

[**\Swagger\Client\Model\RefuelsList**](../Model/RefuelsList.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **getVehiclesStoppedConsumptions**
> \Swagger\Client\Model\StopppedConsumptionList getVehiclesStoppedConsumptions($vehicle_id, $filters_started_at, $filters_ended_at, $page, $per_page)



Get vehicles stopped-consumptions

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$vehicle_id = 56; // int | vehicle identifier
$filters_started_at = "filters_started_at_example"; // string | filters[started_at]
$filters_ended_at = "filters_ended_at_example"; // string | filters[ended_at]
$page = "page_example"; // string | page
$per_page = "per_page_example"; // string | per_page

try {
    $result = $apiInstance->getVehiclesStoppedConsumptions($vehicle_id, $filters_started_at, $filters_ended_at, $page, $per_page);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->getVehiclesStoppedConsumptions: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **int**| vehicle identifier |
 **filters_started_at** | **string**| filters[started_at] | [optional]
 **filters_ended_at** | **string**| filters[ended_at] | [optional]
 **page** | **string**| page | [optional]
 **per_page** | **string**| per_page | [optional]

### Return type

[**\Swagger\Client\Model\StopppedConsumptionList**](../Model/StopppedConsumptionList.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **refreshApiToken**
> \Swagger\Client\Model\UserProfile refreshApiToken()



Refresh api token

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->refreshApiToken();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->refreshApiToken: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\Swagger\Client\Model\UserProfile**](../Model/UserProfile.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateNotificationSettings**
> \Swagger\Client\Model\NotificationSettings updateNotificationSettings($body)



Update notification settings

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \Swagger\Client\Model\UpdateNotificationSettings(); // \Swagger\Client\Model\UpdateNotificationSettings | 

try {
    $result = $apiInstance->updateNotificationSettings($body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->updateNotificationSettings: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\Swagger\Client\Model\UpdateNotificationSettings**](../Model/UpdateNotificationSettings.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\NotificationSettings**](../Model/NotificationSettings.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateUserProfile**
> \Swagger\Client\Model\UserProfile updateUserProfile($profile_id, $body)



Update user profile

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$profile_id = 56; // int | ID
$body = new \Swagger\Client\Model\UpdateProfile(); // \Swagger\Client\Model\UpdateProfile | 

try {
    $result = $apiInstance->updateUserProfile($profile_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->updateUserProfile: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **profile_id** | **int**| ID |
 **body** | [**\Swagger\Client\Model\UpdateProfile**](../Model/UpdateProfile.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\UserProfile**](../Model/UserProfile.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateVehicle**
> \Swagger\Client\Model\Vehicle updateVehicle($vehicle_id, $body)



Update vehicle

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$vehicle_id = 56; // int | ID
$body = new \Swagger\Client\Model\UpdateVehicle(); // \Swagger\Client\Model\UpdateVehicle | 

try {
    $result = $apiInstance->updateVehicle($vehicle_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->updateVehicle: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **vehicle_id** | **int**| ID |
 **body** | [**\Swagger\Client\Model\UpdateVehicle**](../Model/UpdateVehicle.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\Vehicle**](../Model/Vehicle.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **updateVehicleCategory**
> \Swagger\Client\Model\VehicleCategory updateVehicleCategory($category_id, $body)



Update vehicle category

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearerAuth
    $config = Swagger\Client\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new Swagger\Client\Api\V2Api(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$category_id = 56; // int | ID
$body = new \Swagger\Client\Model\UpdateVehicleCategory(); // \Swagger\Client\Model\UpdateVehicleCategory | 

try {
    $result = $apiInstance->updateVehicleCategory($category_id, $body);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling V2Api->updateVehicleCategory: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category_id** | **int**| ID |
 **body** | [**\Swagger\Client\Model\UpdateVehicleCategory**](../Model/UpdateVehicleCategory.md)|  | [optional]

### Return type

[**\Swagger\Client\Model\VehicleCategory**](../Model/VehicleCategory.md)

### Authorization

[bearerAuth](../../README.md#bearerAuth)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

