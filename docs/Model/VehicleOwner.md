# VehicleOwner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstname** | **string** |  | [optional] 
**lastname** | **string** |  | [optional] 
**company** | **string** |  | [optional] 
**company_short_name** | **string** |  | [optional] 
**id** | **int** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

