# CreateProfileUser

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**firstname** | **string** |  | [optional] 
**lastname** | **string** |  | [optional] 
**email** | **string** |  | [optional] 
**phone** | **string** |  | [optional] 
**comment** | **string** |  | [optional] 
**groups** | **int[]** |  | [optional] 
**company** | **string** |  | [optional] 
**company_short_name** | **string** |  | [optional] 
**notify_timezone** | **int** |  | [optional] 
**lang** | **int** |  | [optional] 
**parent** | **int** |  | [optional] 
**nav_system_access_data** | [**\Swagger\Client\Model\CreateProfileUserNavSystemAccessData**](CreateProfileUserNavSystemAccessData.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

