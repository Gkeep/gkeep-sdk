# StopppedConsumptionListList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**created_at** | **int** |  | [optional] 
**longitude** | **string** |  | [optional] 
**latitude** | **string** |  | [optional] 
**finished_at** | **int** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

