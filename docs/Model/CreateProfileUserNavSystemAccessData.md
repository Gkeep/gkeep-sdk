# CreateProfileUserNavSystemAccessData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account** | **string** |  | [optional] 
**password** | **string** |  | [optional] 
**username** | **string** |  | [optional] 
**apikey** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

