# VehicleTanks

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**tank_type** | [**\Swagger\Client\Model\VehicleTankType**](VehicleTankType.md) |  | [optional] 
**vehicle** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

