# VehicleListSensor

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**gkeep_id** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

