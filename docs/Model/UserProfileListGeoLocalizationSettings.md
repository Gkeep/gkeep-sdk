# UserProfileListGeoLocalizationSettings

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**service** | **string** |  | [optional] 
**app_id** | **string** |  | [optional] 
**api_key** | **string** |  | [optional] 
**login** | **string** |  | [optional] 
**password** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

