# VehicleVersionList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**list** | [**\Swagger\Client\Model\VehicleVersionListList[]**](VehicleVersionListList.md) |  | [optional] 
**count** | **int** |  | [optional] 
**count_per_page** | **int** |  | [optional] 
**page_count** | **int** |  | [optional] 
**name_page_parameter** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

