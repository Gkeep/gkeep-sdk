# UserProfileListList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email** | **string** |  | [optional] 
**groups** | [**\Swagger\Client\Model\UserProfileListGroups[]**](UserProfileListGroups.md) |  | [optional] 
**locked** | **bool** |  | [optional] 
**firstname** | **string** |  | [optional] 
**lastname** | **string** |  | [optional] 
**phone** | **string** |  | [optional] 
**parent_id** | **int** |  | [optional] 
**company** | **string** |  | [optional] 
**id** | **int** |  | [optional] 
**lang** | [**\Swagger\Client\Model\UserProfileLang**](UserProfileLang.md) |  | [optional] 
**geo_localization_settings** | [**\Swagger\Client\Model\UserProfileListGeoLocalizationSettings**](UserProfileListGeoLocalizationSettings.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

