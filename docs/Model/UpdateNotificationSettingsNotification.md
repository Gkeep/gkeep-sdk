# UpdateNotificationSettingsNotification

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alert_1** | [**\Swagger\Client\Model\UpdateNotificationSettingsNotificationAlert1**](UpdateNotificationSettingsNotificationAlert1.md) |  | [optional] 
**alert_2** | [**\Swagger\Client\Model\UpdateNotificationSettingsNotificationAlert2**](UpdateNotificationSettingsNotificationAlert2.md) |  | [optional] 
**alert_3** | [**\Swagger\Client\Model\UpdateNotificationSettingsNotificationAlert3**](UpdateNotificationSettingsNotificationAlert3.md) |  | [optional] 
**alert_4** | [**\Swagger\Client\Model\UpdateNotificationSettingsNotificationAlert4**](UpdateNotificationSettingsNotificationAlert4.md) |  | [optional] 
**alert_5** | [**\Swagger\Client\Model\UpdateNotificationSettingsNotificationAlert4**](UpdateNotificationSettingsNotificationAlert4.md) |  | [optional] 
**alert_6** | [**\Swagger\Client\Model\UpdateNotificationSettingsNotificationAlert4**](UpdateNotificationSettingsNotificationAlert4.md) |  | [optional] 
**alert_8** | [**\Swagger\Client\Model\UpdateNotificationSettingsNotificationAlert4**](UpdateNotificationSettingsNotificationAlert4.md) |  | [optional] 
**alert_9** | [**\Swagger\Client\Model\UpdateNotificationSettingsNotificationAlert9**](UpdateNotificationSettingsNotificationAlert9.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

