# VehicleVersionListList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **string** |  | [optional] 
**brand** | [**\Swagger\Client\Model\VehicleBrandListInner**](VehicleBrandListInner.md) |  | [optional] 
**model** | [**\Swagger\Client\Model\VehicleModelListList**](VehicleModelListList.md) |  | [optional] 
**started_at** | **int** |  | [optional] 
**ended_at** | **string** |  | [optional] 
**tonnage** | **int** |  | [optional] 
**power** | **int** |  | [optional] 
**power_cv** | **int** |  | [optional] 
**axes** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

