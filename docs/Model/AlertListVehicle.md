# AlertListVehicle

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**owner** | [**\Swagger\Client\Model\AlertListVehicleOwner**](AlertListVehicleOwner.md) |  | [optional] 
**name** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

