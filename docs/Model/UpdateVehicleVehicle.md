# UpdateVehicleVehicle

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | [optional] 
**nav_system_object_uid** | **string** |  | [optional] 
**release_year** | **int** |  | [optional] 
**registration** | **string** |  | [optional] 
**type** | **int** |  | [optional] 
**brand** | **int** |  | [optional] 
**version** | **int** |  | [optional] 
**model** | **int** |  | [optional] 
**category** | **int** |  | [optional] 
**initial_consumption** | **float** |  | [optional] 
**country** | **int** |  | [optional] 
**nav_system** | **int** |  | [optional] 
**tanks** | [**\Swagger\Client\Model\UpdateVehicleVehicleTanks[]**](UpdateVehicleVehicleTanks.md) |  | [optional] 
**tank_position** | **int** |  | [optional] 
**technical** | **int** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

