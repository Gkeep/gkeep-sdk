# Vehicle

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**photo_path** | **string** |  | [optional] 
**total_day_off** | **int** |  | [optional] 
**id** | **int** |  | [optional] 
**brand** | [**\Swagger\Client\Model\VehicleBrand**](VehicleBrand.md) |  | [optional] 
**model** | [**\Swagger\Client\Model\VehicleModel**](VehicleModel.md) |  | [optional] 
**version** | [**\Swagger\Client\Model\VehicleVersion**](VehicleVersion.md) |  | [optional] 
**comment** | **string** |  | [optional] 
**country** | [**\Swagger\Client\Model\VehicleCountry**](VehicleCountry.md) |  | [optional] 
**owner** | [**\Swagger\Client\Model\VehicleOwner**](VehicleOwner.md) |  | [optional] 
**driver** | [**\Swagger\Client\Model\VehicleDriver**](VehicleDriver.md) |  | [optional] 
**groups** | [**null[]**](.md) |  | [optional] 
**initial_consumption** | **float** |  | [optional] 
**name** | **string** |  | [optional] 
**nav_system_object_uid** | **string** |  | [optional] 
**photo** | **string** |  | [optional] 
**registration** | **string** |  | [optional] 
**release_year** | **int** |  | [optional] 
**type** | [**\Swagger\Client\Model\VehicleCategoryType**](VehicleCategoryType.md) |  | [optional] 
**technical** | [**\Swagger\Client\Model\VehicleTechnical**](VehicleTechnical.md) |  | [optional] 
**nav_system** | [**\Swagger\Client\Model\VehicleNavSystem**](VehicleNavSystem.md) |  | [optional] 
**sensor** | [**\Swagger\Client\Model\VehicleSensor**](VehicleSensor.md) |  | [optional] 
**tanks** | [**\Swagger\Client\Model\VehicleTanks[]**](VehicleTanks.md) |  | [optional] 
**tank_position** | [**\Swagger\Client\Model\VehicleTankPosition**](VehicleTankPosition.md) |  | [optional] 
**is_maintenance_enabled** | **bool** |  | [optional] 
**category** | [**\Swagger\Client\Model\VehicleCategory**](VehicleCategory.md) |  | [optional] 
**ptac** | **string** |  | [optional] 
**eligible_for_ticpe** | **string** |  | [optional] 
**eligible_for_tsvr** | **string** |  | [optional] 
**national_type** | **string** |  | [optional] 
**fuel_type** | **string** |  | [optional] 
**property** | **string** |  | [optional] 
**box_id** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

