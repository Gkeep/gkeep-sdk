# AlertListList

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fuel_level** | **float** |  | [optional] 
**fuel_level_on_raise_start** | **float** |  | [optional] 
**action_status** | **int** |  | [optional] 
**id** | **int** |  | [optional] 
**nav_system_object_uid** | **string** |  | [optional] 
**created_at** | **int** |  | [optional] 
**is_closed** | **bool** |  | [optional] 
**description** | **string** |  | [optional] 
**vehicle** | [**\Swagger\Client\Model\AlertListVehicle**](AlertListVehicle.md) |  | [optional] 
**code** | [**\Swagger\Client\Model\AlertListCode**](AlertListCode.md) |  | [optional] 
**driver** | [**\Swagger\Client\Model\AlertListDriver**](AlertListDriver.md) |  | [optional] 
**level** | [**\Swagger\Client\Model\AlertListLevel**](AlertListLevel.md) |  | [optional] 
**status** | **int** |  | [optional] 
**is_maintenance_enabled** | **bool** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

